---
title: "Hummus"
date: 2019-04-12T14:10:00+02:00
image: "hummus.jpg"
tags: [chickpeas, pepper, lime, tahini, garlic,
       cumin, parsley, jalapeno]
---
* 400g canned chickpeas
* 100g grilled pepper
* juice from 1 lime
<!--more-->
* 3 ts tahini (grilled sesame seeds + 3 ts olive oil)
* 3 ts liquid from the chickpeas can
* 1 garlic clove
* ½ ts ground cumin
* ½ ts cayenne pepper
* ¼ ts salt
* parsley
* red jalapeno pepper

Mix well in a blender and serve chilled.

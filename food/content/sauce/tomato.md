---
title: "Tomato salsa"
date: 2019-03-17T13:00:00+02:00
image: "tomato.jpg"
tags: [tomato, onion, garlic, vinegar, lime, coriander,
       avocado]
---
* 4-6 medium tomatoes, peeled and finely chopped
* ½ red onion, very finely chopped
* 1 small garlic clove, chopped
<!--more-->
* small splash white wine vinegar
* ½ lime, juice only
* ½ bunch coriander
* 1 avocado

Blend in a food processor and serve chilled.

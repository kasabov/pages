---
title: "Chimichurri"
date: 2020-04-11T14:10:00+02:00
image: "chimichurri.jpg"
tags: [olive oil, vinegar, parsley, jalapeno, garlic, oregano]
---
* ½ cup olive oil
* ½ cup parsley
* 2 ts red wine vinegar
<!--more-->
* 1 jalapeno
* 2 cloves garlic
* ½ ts salt
* ½ ts black pepper

Blend everything together. Serve over grilled salmon or meat.

Source [cafedelites.com](https://cafedelites.com/authentic-chimichurri-uruguay-argentina/)

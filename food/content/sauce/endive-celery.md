---
title: "Celery endive sauce"
date: 2019-04-12T13:00:00+02:00
image: "endive-celery.jpg"
tags: [endive, celery, nutmeg, butter, cream,
       olive oil, ginger, lime, garlic, chillies]
---
* 6 leaves endive
* 3 ribs of celery
* ⅓ grated nutmeg
<!--more-->
* 2 ts melted butter
* 2 ts cream
* 5 ts olive oil
* 1 inch ginger root
* ½ lime juice
* garlic
* salt, pepper, chillies, bean spices

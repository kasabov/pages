---
title: "Cornbread"
date: 2018-12-29T13:57:00+02:00
image: "cornbread.jpg"
tags: [butter, flour, corn flour, eggs, baking soda, baking power,
       lemon, sugar, corn, bell pepper, cumin, caraway seeds]
---
* 1 ½ cup flour
* 1 ½ cup corn flour (yellow)
* ½ cup butter
<!--more-->
* 2 eggs
* ¼ cup chopped parsley
* 1 t.s baking soda
* 1 t.s baking powder
* 1 t.s salt
* 1 t.s lemon
* 2 t.s sugar
* 1 cup corn
* 1 red chopped pepper (not on the picture)
* ½ t.s black pepper
* ¼ t.s cumin
* ¼ t.s caraway seeds

Bake 50 mins at 150ºC. Serve with ketchup and creme fraiche. 

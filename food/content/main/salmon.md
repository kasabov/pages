---
title: "Salmon"
date: 2019-03-17T12:00:00+02:00
image: "salmon.jpg"
tags: [salmon, bread crumbs, cream cheese, lime, chives]
---
* 4 salmon fillets
* 100g cream cheese
* zest of 1 lime
<!--more-->
* 50g bread crumbs
* 20g chopped chives

Cover the salmon fillets with cream cheese. Then put the bread
crumbs mix on top and bake in the oven for 25 mins at 180ºC.

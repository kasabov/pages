---
title: "Lentils salad"
date: 2020-08-10T10:00:00+02:00
image: "lentils-salad.jpg"
tags: [cucumber, tomatoes, spring onion, mint, olive oil, lime, cumin, lentils, feta, pomegranate]
---
* 200g lentils, boiled for 25 minutes
* ½ cucumber
* 5 cherry tomatoes, cut in quarters
<!--more-->
* 2 spring onions
* 7g finely chopped mint
* 75g feta cheese
* 1 lime's juice
* 1 pomegranate seeds
* olive oil, salt and cumin
* (optional) 1 mozzarella and 1 red pepper

Mix everything and serve cold.

Source: [ah.nl](https://www.ah.nl/allerhande/recept/R-R1193968/linzensalade-met-witte-kaas-en-granaatappelpitjes)

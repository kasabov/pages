---
title: "Omelette cake"
date: 2020-08-09T10:00:00+02:00
image: "omelette-cake.jpg"
tags: [feta, eggs, oil, flower, pepper]
---
* 1 cup crumbled feta cheese
* 4 eggs
* 1 cup sunflower oil
<!--more-->
* 4 cups flower
* 1 pack baking powder
* 1 chopped red pepper
* salt and pepper
* 1 cup yoghurt

Bake in a cake form for 30 minutes at 180ºC.

---
title: "Puff pastry with bacon and cheese"
date: 2019-05-20T13:00:00+02:00
image: "puff-pastry-bacon-cheese.jpg"
tags: [puff pastry, bacon, cheese]
---
* 450g puff pastry sheets
* 250g grated cheese
* 200g bacon strips
<!--more-->

Lay half of the sheets on baking paper in an oven tray.
Spread the grated cheese and cover with the other half of
puff pastry sheets. Put bacon strips on top. Cook for 30 
minutes at 180˚C.

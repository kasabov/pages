---
title: "Green shakshuka"
date: 2018-12-30T12:17:00+02:00
image: "shakshuka.jpg"
tags: [asparagus, olive oil, courgette, spinach, eggs]
---
* 350g green asparagus
* 1 ts olive oil
* 400g grated courgette
<!--more-->
* 100g spinach
* 4 eggs

Bake 2 mins in a pan, add the eggs and steam for 15 mins with lid on.
Serve with [ricotta-mint sauce]({{< relref "ricotta-mint.md" >}}).

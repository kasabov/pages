---
title: "Omelette du fromage"
date: 2019-07-28T08:00:00+02:00
image: "omelette-du-fromage.jpg"
tags: [egg, tomato, cheese, butter, olive oil, chives]
---
* 3 whisked eggs
* 1 tomato
* 50g cheese, in cubes
<!--more-->
* 2 tbsp olive oil
* 1 ts butter
* chives
* parmigiano salt
* black pepper

Heat the olive oil in a pan. Add chopped tomato cubes and salt. Add eggs with black pepper, then the cheese cubes. Add bits of butter on 4-5 places. Add more salt.

Flip in half and serve with chives.

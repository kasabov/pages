---
title: "Spanish dish with sausages"
date: 2019-01-07T23:06:00+02:00
image: "spanish-sausages.jpg"
tags: [gnocchi, olive oil, sausage, garlic, cayenne pepper, tomato paste,
       cherry tomatoes, olives, spinach, parsley, jalapeno]
---
* 500g gnocchi
* 2 ts olive oil
<!--more-->

Roast in a pan for 15 mins. 

* 250g beef chipolata sausages
* 1 clove of garlic
* 1 ts cayenne pepper
* 1 ts olive oil

Roast in a pot for 2 mins. Then add: 

* 200ml water
* 180g green olives without pits
* 30g tomato paste
* 250g cherry tomatoes
* 100g spinach
* 7.5g parsley
* pepper and salt
* 1 red jalapeno pepper

Simmer everything in a pot for another 5 mins.
Serve with the gnocchi.

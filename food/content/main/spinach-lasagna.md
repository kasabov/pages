---
title: "Spinach lasagna"
date: 2019-01-14T17:35:00+02:00
image: "spinach-lasagna.jpg"
tags: [olive oil, onion, mushrooms, garlic, spinach, walnuts,
       goat cheese, lasagna sheets]
---
* 2 ts olive oil
* 1 big onion
* 400g mushrooms
<!--more-->
* pepper and salt

Bake for 5 minutes in a tall pot.

* 2 crushed garlic cloves
* 1 kg spinach

Simmer for a couple of minutes until the spinach has softened.

* 300g grated goat cheese

Lay ¼ of the mix over lasagna sheets in an oiled 1.5l bowl.
Sprinkle ¼ of the goat cheese. Repeat 3 times more.
Add walnuts on top and bake for 25 minutes at 180°C.

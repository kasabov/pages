---
title: "Curry"
date: 2018-12-29T14:47:00+02:00
image: "curry.jpg"
tags: [olive oil, garlic, ginger, onions, bell pepper, chillies,
       cinnamon, turmeric, curry, chicken, beef, coriander, chickpeas, celery]
---
* 50ml water
* 100ml olive oil
* 5 garlic cloves
<!--more-->
* 25g fresh root ginger
* 2 small onions
* 1 large green pepper
* 2 large red chillies
* ½ ts cinnamon
* ½ ts turmeric
* 2 ts curry powder
* 1 ts salt
* 1 ts honey
* 2 ts lemon

Blend in a food processor. Then roast in a deep pot:

* 800g chicken or beef: add 2 spoons of the above mix and roast till golden brown ~6 mins
* 1 onion: roast for another 2 mins, then add the rest of the curry and simmer for 10 mins if chicken or 2 hours if beef
* 15g coriander
* 400g chickpeas
* 4 stalks celery

Serve with rice or couscous and yogurt. 

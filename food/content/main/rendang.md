---
title: "Rendang"
date: 2020-11-13T10:00:00+02:00
image: "rendang.jpg"
tags: [lemon grass, onions, garlic, ginger, chillies, oil, cumin, coriander,
       turmeric, beef, coconut milk, bay leaves, cinnamon stick, sugar, lime,
       soy sauce, coconut flakes]
---
* 2 lemon grass stalks
* 3 red onions
* 6 garlic cloves
<!--more-->
* 4cm ginger root
* 3 chillies
* 3 tbsp sunflower oil
* 2 tbsp cumin
* 2 tbsp dry coriander
* 1 tsp turmeric

Blend well in a food processor. Fry the paste for 3-4 minutes in a deep pot.

* 1.5 kg beef into 3cm cubes

Add to the paste and sear for 5 minutes.

* 400ml coconut milk
* 800ml water
* 4 bay (or lime) leaves
* 1 cinnamon stick
* 1 tbsp brown sugar
* 1 lime, juiced
* 2 tbsp dark soy sauce
* 2 tsp sea salt
* ½ tsp black pepper

Add all the above to the pot and simmer for 2½ hours until sauce has thickened.
Serve with rice and roasted coconut flakes. 

Source: [bbc.co.uk/food](https://www.bbc.co.uk/food/recipes/beef_rendang_62793)

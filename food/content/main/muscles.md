---
title: "Muscles"
date: 2019-01-07T23:34:00+02:00
image: "muscles.jpg"
tags: [garlic, celery, carrot, olive oil, wine, muscles,
       parsley, sage]
---
* 3 cloves garlic
* 3 celery stalks
<!--more-->
* 1 carrot
* 1 ts olive oil

Roast for 5 mins in a deep pot.

* 150ml dry white wine
* 2kg muscles
* 20g parsley
* 6 leaves sage

Simmer for another 5 mins on high heat with the lid on.

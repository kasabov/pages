---
title: "Gazpacho"
date: 2019-01-07T23:20:00+02:00
image: "gazpacho.jpg"
tags: [onion, garlic, olive oil, tomato, tomato paste,
       jalapeno, cucumber, bell pepper, lime, parsley, ginger root,
       feta, avocado]
---
* 1 big onion
* 2 cloves garlic
* 350 ml water
<!--more-->
* 50ml olive oil
* 600g roma tomatoes
* 60g tomato paste
* 1 rode/jalapeno pepper
* 1 cucumber
* 1 green pepper
* 1 lime juice
* parsley
* black pepper and salt
* 1cm ginger root

Serve with feta cheese and avocado or shrimps.
Can be boiled by adding extra water and vegetable bouillon.

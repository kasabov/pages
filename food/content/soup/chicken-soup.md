---
title: "Chicken soup"
date: 2019-09-13T14:37:00+02:00
image: "chicken-soup.jpg"
tags: [olive oil, onion, carrots, chicken, parsley, potatoes, harissa, canned tomatoes,
       tomatoes, cayenne pepper, spring onion]
---
* 1 tbsp olive oil
* 700g chicken
* 1 tbsp harissa
<!--more-->
Cook for 3 mins in a deep pot and take out on a plate. In the same pot:

* 1 tbsp olive oil
* 1 white onion, diced small or 1 leek
* 500g carrots

Cook for 5 mins and add:

* 1kg potatoes
* 300g canned tomatoes
* 1l water or more
* 1 tbsp salt, 1 ts cayenne pepper, 1 ts black pepper
* 2 tbsp harissa

Simmer for 10 mins until potatoes are cooked. In a separate pan:

* 1 big chopped tomato
* 3 spring onions

Cook for 5 mins and add them to the soup together with:

* 20g chopped parsley

Optionally, serve with a spoon of yogurt. 

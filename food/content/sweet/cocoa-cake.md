---
title: "Cocoa cake"
date: 2020-07-01T10:00:00+02:00
image: cocoa-cake.jpg
tags: [cocoa, flour, sugar, egg, olive oil, aroma]
---

* 200g sugar
* 150ml olive oil
* 3 eggs
<!--more-->

Blend with a hand mixer for 2 mins and add:

* 150g flour
* ½ ts salt
* ½ ts baking soda

Mix again. In a separate bowl, mix the following:

* 50g cocoa
* 120ml hot water
* optional: 1 ts aroma

Add the above mix to the flour. Bake 40 mins at 180°.

Source [Nigella Lawson](https://www.nigella.com/recipes/chocolate-olive-oil-cake)

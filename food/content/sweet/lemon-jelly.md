---
title: "Lemon jelly"
date: 2019-04-19T15:40:00+02:00
image: lemon-jelly.jpg
tags: [lime, sugar, gelatin]
---
* 6 juiced limes, 3 of them grated
* 1 cup crystal sugar
* 2 cups water
<!--more-->
* 4 gelatin leaves
* optional: extra cup sugar, fruit

Gelatine leaves in cold water for 5 min.
Squeeze the water out, add them to lemon mix.
Stir until hot and all ingredients get dissolved.
Optionally, sieve. Refrigerate for 24 hours.

Serve with the lime zest from sieve and mint leaves or cake.

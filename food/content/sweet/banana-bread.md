---
title: "Banana bread"
date: 2019-04-19T14:30:00+02:00
image: banana-bread.jpg
tags: [banana, flour, egg, milk, sugar, butter, aroma, soda, pecans]
---
* 3 ripe bananas
* 3 cups flour
* 2 eggs (or ½ cup milk)
<!--more-->
* 1 cup caster sugar
* ½ cup butter
* 1 t.s vanilla aroma
* 1 t.s baking soda
* 1 t.s salt
* 100 gram pecan nuts, chopped in the mix and for decoration on top
* (variation 1) blueberries, 2 t.s. extra sugar
* (variation 2) 3 t.s coffee, 1 t.s rum aroma instead of vanilla, 2 t.s extra sugar

Bake 50 mins at 150°C.

Optional frosting:

* ½ cup heavy cream 20%
* 1 cup powdered sugar
* ½ t.s vanilla aroma

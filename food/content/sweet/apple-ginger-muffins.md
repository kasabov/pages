---
title: "Apple ginger muffins"
date: 2019-04-12T14:30:00+02:00
image: apple-ginger-muffins.jpg
tags: [flour, butter, sugar, apple, milk, soda, aroma,
       lemon, cinnamon, black pepper, egg, ginger, chocolate]
---
* 3 cup flour
* 1 cup butter
* 1 cup caster sugar
<!--more-->
* 1 grated apple
* ½ cup milk
* 1 ts baking soda
* 1 ts vanilla aroma
* 1 ts lemon
* ½ ts cinnamon
* ½ ts salt
* ¼ ts black pepper
* 1 egg
* 1 inch grated ginger root
* 200g chocolate
* optional: apricots

Bake for 30 mins at 150°C. Serve with powdered sugar or “Van Gilse apple syrup”.

Optional frosting: 1 cup heavy cream, ½ cup powdered sugar, pinch of vanilla aroma.


---
title: "Cheesecake"
date: 2019-04-19T15:35:00+02:00
image: cheesecake.jpg
tags: [biscuits, butter, sugar, cream cheese, egg, aroma, lime, cream]
---
Crust:

* 2 cups crumbled biscuits
* ⅓ cup butter
<!--more-->

* ⅓ cup powdered sugar
* ½ ts salt

Filling:

* 500g cream cheese (at room temperature)
* 2 eggs
* 1½ cups sugar
* 1 ts aroma
* 4 ts lime juice
* optional: 1 cup cooking cream

Bake 1h at 150°. Let it cool down for 10 mins and refrigerate for 4 hours.

Serve with cacao, raspberries and mint leaves or [lemon jelly]({{< relref "lemon-jelly.md" >}}).

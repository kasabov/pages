---
title: "Orange cake"
date: 2019-04-19T15:35:00+02:00
image: orange-cake.jpg
tags: [orange, egg, honey, almond, soda]
---
* 2 organic Valencia oranges boiled for 1 hour
* 4 eggs
* 1 cup of honey
<!--more-->
* 2½ cups of blanched almond flour
* 1 teaspoon of baking soda
* ½ teaspoon of salt

Bake for 40 mins at 150°C.

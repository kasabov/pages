---
title: "Tiramisu"
date: 2019-04-19T15:30:00+02:00
image: tiramisu.jpg
tags: [egg, sugar, amaretto, aroma, mascarpone, lady fingers, cocoa, cream,
       espresso]
---
* 4 egg yolks
* 1 cup sugar
* 2 ts amaretto (or Marsala wine)
<!--more-->
* 1 ts vanilla aroma
* 400 gr mascarpone
* (optional) ½ cup heavy cream

Mix well in a bowl for at least 10 minutes.

* 4 egg whites

Mix in a bowl for 3 minutes and add to the mascarpone mixture.

* lady fingers
* 100 ml espresso 
* 2 t.s amaretto (or Marsala wine)
* 1 t.s vanilla aroma
* 1 t.s sugar

Drop the lady fingers for 4 seconds in the above mix and lay them
in a dish. Cover with the mascarpone mixture and then with cocoa powder.

Refrigerate for at least 6 hours before serving.

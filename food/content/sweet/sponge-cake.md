---
title: Sponge cake
date: 2020-06-18T16:00:00+01:00
image: sponge-cake.jpg
tags: [eggs, sugar, flower, oil, milk, vanilla]
---
* 6 eggs, split in yolks and whites
* ½ cup sugar

<!--more-->

Beat the egg whites with hand mixer for 2 mins, add the sugar and mix for another minute.

* 6 egg yolks 
* 1 cup flower
* ½ cup sunflower oil
* ½ cup milk
* 1 ts vanilla aroma
* ¼ ts salt

Beat everything well with the mixer. Fold in the sweetened egg whites. Bake for 1 hour at 150°C.
I'm still working out how to prevent the cake from shrinking at the end. 🤷

Source [mrspskitchen.net](http://www.mrspskitchen.net/taiwanese-sponge-cake-castella-cake-%E5%8F%A4%E6%97%A9%E5%91%B3%E8%9B%8B%E7%B3%95/)
